import sublime, sublime_plugin
import re

# WORDS
# 0x000a212f
# Bx0_10_33_47
# Wx663855
# Hx10_8495
# Bx0_10_33_47
# 0x000a212f
    


# DOUBLE WORDS
# Hx0_16 Hx0_16
# Bx1_35_69_103 Bx137_171_205_239
# Hx291_17767 Wx2309737967
# Hx291_17767 Hx35243_52719
# 0x000000eb

# [
#     {
#         "keys": ["shift+enter"],
#         "command": "hexa_parse_all",
#         "context":   [    { "key": "allSelectionsEmpty", "value": true }   ]
#     },
#     {
#         "keys": ["shift+enter"],
#         "command": "hexa_parse_one",
#         "context":   [    { "key": "allSelectionsEmpty", "value": false }   ]
#     }
# ]

log2_max_size = {
    'W' : 8,
    'H' : 4,
    'B' : 2
}

change_base_from_to = {
    '0' : 'W',
    'W' : 'H',
    'H' : 'B',
    'B' : '0'
}

def toHexa(data,base):
    n = log2_max_size[base]
    #print(data[2:].split("_"))
    return "0x"+''.join( hex(int(hw))[2:].rjust(n,'0') for hw in data[2:].split("_"))

def fromHexa(data,base):
    size = log2_max_size[base]
    n = int(8/size)
    #print("split %s into %d"%(data,n))
    fields = [ data[(2+i*size):(2+(i+1)*size)] for i in range(n) ]
    #print(fields)
    return base+"x"+"_".join("%d"%int(f,16) for f in fields )

def changeBase(s_in,baseTo):
    baseFrom = s_in[0]
    
    if baseFrom=="0" and len(s_in)>10:
        return s_in[0:-8]+" 0x"+s_in[-8:]
        
    hexa = s_in if baseFrom == "0" else toHexa(s_in,baseFrom)

    s_out = hexa if baseTo == "0" else fromHexa(hexa,baseTo)

    #print("Convert From %s To %s: %s -> %s -> %s"%(baseFrom,baseTo,s_in,hexa,s_out))
    
    return s_out




class HexaParseAllCommand(sublime_plugin.TextCommand):
    def run(self, edit):

        sel = self.view.find_all(r'[0|H|W|B]x[0-9a-fA-F_]+')
        sel.reverse()
        for i,region in enumerate(sel):
            s = self.view.substr(region)
            baseTo = change_base_from_to[ s[0] ]
            # print("[%d] region <"%i+s+">")
            s2 = changeBase(s,baseTo)
            #print("[%d] region <"%i+s+"> <"+s2+">")
            self.view.replace(edit, region, s2)
            # print(s2)

class HexaParseOneCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        #self.view.insert(edit, 0, "Hello, World!")

        for region in self.view.sel():

            if not region.empty():
                # Get the selected text
                s = self.view.substr(region)

                if re.match(r'0x[0-9a-fA-F]{1,8}', s):
                    if len(s)>(2+2*8):
                        print("[ERROR] HexaParser does not support input > 64 bits")
                        return

                    self.view.replace(edit, region, changeBase(s,'W'))

                elif re.match(r'Wx[0-9\s]{1,11}', s):
                    self.view.replace(edit, region, changeBase(s,'H'))
                elif re.match(r'Hx[0-9\s]{1,5}_[0-9\s]{1,5}', s):
                    self.view.replace(edit, region, changeBase(s,'B'))
                elif re.match(r'Bx[0-9\s]{1,3}_[0-9\s]{1,3}_[0-9\s]{1,3}_[0-9\s]{1,3}', s):
                    self.view.replace(edit, region, changeBase(s,'0'))
                else:
                	print( "format not found !!"  )

